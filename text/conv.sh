for file in *.lat 
do
    iconv -f ISO-8859-15 -t utf8 "$file" > "$file.tmp" &&
    mv -f "$file.tmp" "$file"
done
