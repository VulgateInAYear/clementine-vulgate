package main

import (
	"fmt"
	"os"
	"bufio"
	"context"
	"strings"
	"strconv"

	"github.com/jackc/pgx/v4"
)

var bibleName = "Clementine Vulgate"
var bibleSlug = "clementine"

var books = [][]string {
	{"Genesis", "genesis", "Gn.lat", "50"},
	{"Exodus", "exodus", "Ex.lat", "40"},
	{"Leviticus", "leviticus", "Lv.lat", "27"},
	{"Numeri", "numeri", "Nm.lat", "36"},
	{"Deuteronomium", "deuteronomium", "Dt.lat", "34"},
	{"Josue", "josue", "Jos.lat", "24"},
	{"Judices", "judices", "Jdc.lat", "21"},
	{"Ruth", "ruth", "Rt.lat", "4"},
	{"Regum I", "regum-i", "1Rg.lat", "31"},
	{"Regum II", "regum-ii", "2Rg.lat", "24"},
	{"Regum III", "regum-iii", "3Rg.lat", "22"},
	{"Regum IV", "regum-iv", "4Rg.lat", "25"},
	{"Paralipomenon I", "paralipomenon-i", "1Par.lat", "29"},
	{"Paralipomenon II", "paralipomenon-ii", "2Par.lat", "36"},
	{"Esdræ", "esdrae", "Esr.lat", "10"},
	{"Nehemiæ", "nehemiae", "Neh.lat", "13"},
	{"Tobiæ", "tobiae", "Tob.lat", "14"},
	{"Judith", "judith", "Jdt.lat", "16"},
	{"Esther", "esther", "Est.lat", "16"},
	{"Job", "job", "Job.lat", "42"},
	{"Psalmi", "psalmi", "Ps.lat", "150"},
	{"Proverbia", "proverbia", "Pr.lat", "31"},
	{"Ecclesiastes", "ecclesiastes", "Ecl.lat", "12"},
	{"Canticum Canticorum", "canticum-canticorum", "Ct.lat", "8"},
	{"Sapientia", "sapientia", "Sap.lat", "19"},
	{"Ecclesiasticus", "ecclesiasticus", "Sir.lat", "51"},
	{"Isaias", "isaias", "Is.lat", "66"},
	{"Jeremias", "jeremias", "Jr.lat", "52"},
	{"Lamentationes", "lamentationes", "Lam.lat", "5"},
	{"Baruch", "baruch", "Bar.lat", "6"},
	{"Ezechiel", "ezechiel", "Ez.lat", "48"},
	{"Daniel", "daniel", "Dn.lat", "14"},
	{"Osee", "osee", "Os.lat", "14"},
	{"Joël", "joel", "Joel.lat", "3"},
	{"Amos", "amos", "Am.lat", "9"},
	{"Abdias", "abdias", "Abd.lat", "1"},
	{"Jonas", "jonas", "Jon.lat", "4"},
	{"Michæa", "michaea", "Mch.lat", "7"},
	{"Nahum", "nahum", "Nah.lat", "3"},
	{"Habacuc", "habacuc", "Hab.lat", "3"},
	{"Sophonias", "sophonias", "Soph.lat", "3"},
	{"Aggæus", "aggaeus", "Agg.lat", "2"},
	{"Zacharias", "zacharias", "Zach.lat", "14"},
	{"Malachias", "malachias", "Mal.lat", "4"},
	{"Machabæorum I", "machabaeorum-i", "1Mcc.lat", "16"},
	{"Machabæorum II", "machabaeorum-ii", "2Mcc.lat", "15"},

	{"Matthæus", "matthaeus", "Mt.lat", "28"},
	{"Marcus", "marcus", "Mc.lat", "16"},
	{"Lucas", "lucas", "Lc.lat", "24"},
	{"Joannes", "joannes", "Jo.lat", "21"},
	{"Actus Apostolorum", "actus-apostolorum", "Act.lat", "28"},
	{"Ad Romanos", "ad-romanos", "Rom.lat", "16"},
	{"Ad Corinthios I", "ad-corinthios-i", "1Cor.lat", "16"},
	{"Ad Corinthios II", "ad-corinthios-ii", "2Cor.lat", "13"},
	{"Ad Galatas", "ad-galatas", "Gal.lat", "6"},
	{"Ad Ephesios", "ad-ephesios", "Eph.lat", "6"},
	{"Ad Philippenses", "ad-philippenses", "Phlp.lat", "4"},
	{"Ad Colossenses", "ad-colossenses", "Col.lat", "4"},
	{"Ad Thessalonicenses I", "ad-thessalonicenses-i", "1Thes.lat", "5"},
	{"Ad Thessalonicenses II", "ad-thessalonicenses-ii", "2Thes.lat", "3"},
	{"Ad Timotheum I", "ad-timotheum-i", "1Tim.lat", "6"},
	{"Ad Timotheum II", "ad-timotheum-ii", "2Tim.lat", "4"},
	{"Ad Titum", "ad-titum", "Tit.lat", "3"},
	{"Ad Philemonem", "ad-philemonem", "Phlm.lat", "1"},
	{"Ad Hebræos", "ad-hebraeos", "Hbr.lat", "13"},
	{"Jacobi", "jacobi", "Jac.lat", "5"},
	{"Petri I", "petri-i", "1Ptr.lat", "5"},
	{"Petri II", "petri-ii", "2Ptr.lat", "3"},
	{"Joannis I", "joannis-i", "1Jo.lat", "5"},
	{"Joannis II", "joannis-ii", "2Jo.lat", "1"},
	{"Joannis III", "joannis-iii", "3Jo.lat", "1"},
	{"Judæ", "judae", "Jud.lat", "1"},
	{"Apocalypsis", "apocalypsis", "Apc.lat", "22"},
}

func main() {
	host := os.Getenv("DBHOST")
	user := os.Getenv("DBUSER")
	pass := os.Getenv("DBPASS")

	// Connect to the database
	conn, err := pgx.Connect(context.Background(), "host=" + host + " port=5432 user=" + user + " password=" + pass + " dbname=viay sslmode=disable")
	if err != nil {
		fmt.Println(err)
		return
	}

	err = conn.Ping(context.Background())
	if err != nil {
		fmt.Println(err)
		return
	}

	// Insert Bible
	_, err = conn.Exec(context.Background(), "INSERT INTO Bibles (Name, Slug) VALUES ($1, $2);", bibleName, bibleSlug)
	if err != nil {
		fmt.Println(err)
		return
	}

	// Get Bible Id
	row := conn.QueryRow(context.Background(), "SELECT Id FROM Bibles WHERE Slug=$1;", bibleSlug)
	var bibleId int
	err = row.Scan(&bibleId)
	if err != nil {
		fmt.Println(err)
		return
	}

	for i, b := range books {
		// Insert Book
		_, err := conn.Exec(context.Background(), "INSERT INTO Books (BibleId, Number, Name, Slug, Chapters) VALUES ($1, $2, $3, $4, $5);", bibleId, i + 1, b[0], b[1], b[3])
		if err != nil {
			fmt.Println(err)
			return
		}

		// Get Book Id
		row := conn.QueryRow(context.Background(), "SELECT Id FROM Books WHERE Slug=$1 AND BibleId=$2;", b[1], bibleId)
		var bookId int
		err = row.Scan(&bookId)
		if err != nil {
			fmt.Println(err)
			return
		}

		file, err := os.Open("text/" + b[2])
		if err != nil {
			fmt.Println(err)
			return
		}

		scanner := bufio.NewScanner(file)
		chapterNum := -1
		chapterId := -1
		for scanner.Scan() {
			line := scanner.Text()

			split := strings.SplitN(line, ":", 2)
			cNum, _ := strconv.Atoi(split[0])
			if cNum > chapterNum {
				chapterNum = cNum

				// Insert Chapter
				_, err := conn.Exec(context.Background(), "INSERT INTO Chapters (BookId, Number) VALUES ($1, $2);", bookId, chapterNum)
				if err != nil {
					fmt.Println(err)
					return
				}

				// Get Chapter Id
				row := conn.QueryRow(context.Background(), "SELECT Id FROM Chapters WHERE BookId=$1 AND Number=$2;", bookId, chapterNum)
				err = row.Scan(&chapterId)
				if err != nil {
					fmt.Println(err)
					return
				}
			}

			split = strings.SplitN(split[1], " ", 2)
			verseNum, _ := strconv.Atoi(split[0])
			// Insert Verse
			_, err := conn.Exec(context.Background(), "INSERT INTO Verses (ChapterId, Number, Content) VALUES ($1, $2, $3);", chapterId, verseNum, split[1])
			if err != nil {
				fmt.Println(err)
				return
			}
		}
		file.Close()
	}
}
